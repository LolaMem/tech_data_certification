
import pandas as pd
import sqlite3
from sqlite3 import Error
from tools import pd_series_to_list


df_nf = pd.read_csv("NF_clean.csv", dtype={'release_year': 'object','averageRating': 'float64', 'year_added': 'int32'})

#Création d'une df pour stocker tous les différents genres en utilisant la fonction pd_series_to_list, 
# pour extraire les genres individuellement de la df_nf, où ils sont regroupés pour chaque production

df_genres = pd.DataFrame(pd_series_to_list(df_nf, "genres"))
#print(df_genres)

# Même pour les acteurs
df_actors = pd.DataFrame(pd_series_to_list(df_nf, "cast"), columns=['name'])

print("ready to insert")
################################################################################"
# Insérer les données dans la base de données db_NF.db
try:
    connection = sqlite3.connect("db_NF.db")

    cursor = connection.cursor()

    cursor.execute("DROP TABLE IF EXISTS production_genre")
    cursor.execute("DROP TABLE IF EXISTS production_actor")
    cursor.execute("DROP TABLE IF EXISTS productions")
    cursor.execute("DROP TABLE IF EXISTS genres")
    cursor.execute("DROP TABLE IF EXISTS actors")

    cursor.execute("CREATE TABLE IF NOT EXISTS genres (id INTEGER NOT NULL PRIMARY KEY, genre TEXT)")
    cursor.execute("CREATE TABLE IF NOT EXISTS actors (id INTEGER NOT NULL PRIMARY KEY, name TEXT)")
    cursor.execute("CREATE TABLE IF NOT EXISTS productions (id TEXT NOT NULL PRIMARY KEY, type TEXT, title TEXT, added_year INTEGER, release_year TEXT, average_rating REAL)")
    cursor.execute("CREATE TABLE IF NOT EXISTS production_genre (id_prod TEXT, id_genre INTEGER, FOREIGN KEY (id_prod) REFERENCES productions (id), FOREIGN KEY (id_genre) REFERENCES genres (id))")
    cursor.execute("CREATE TABLE IF NOT EXISTS production_actor (id_prod TEXT, id_person INTEGER, FOREIGN KEY (id_prod) REFERENCES productions (id), FOREIGN KEY (id_person) REFERENCES actors (id))")
    

    print("tables created")

    data_genres= df_genres.values.tolist()
    #print(data_genres)      # [['International TV Shows'], ['TV Dramas'], ['TV Sci-Fi & Fantasy'], ['Horror Movies'], ...]
    
    data_actors= df_actors.values.tolist()
   
    data_production = df_nf[["show_id","type", "title", "year_added", "release_year", "averageRating"]].values.tolist()
   
    # Insérer des données dans les tables avec des clés primaires
    cursor.executemany("INSERT INTO genres (genre) VALUES (?)", data_genres)
    cursor.executemany("INSERT INTO actors (name) VALUES (?)", data_actors)
    cursor.executemany("INSERT INTO productions VALUES (?, ?, ?, ?, ?, ?)", data_production)
    connection.commit()

    ##########################################################
    
    # Boucle sur df_nf pour effectuer les insertions dans les tables relationnelles : production_genre et production_actor
    for index, row in df_nf.iterrows():
        # Extraction de id production
        id_prod = row["show_id"]
        # Extraction des genres
        genres_row = [x.strip() for x in row["genres"].split(",")]

        for genre in genres_row:
            # Fetch genre ID from the genres table
            cursor.execute("SELECT id FROM genres WHERE genre LIKE ?", (genre,))
            id_genre = cursor.fetchone()

            if id_genre:
                # Extract the id from the tuple, ie: (9,)
                id_genre = id_genre[0]

                #Insert values into relational table production_genre
                cursor.execute("INSERT INTO production_genre VALUES (?, ?)", (id_prod,id_genre))
                connection.commit()
            else:
                print(f"Genre '{genre}' non trouvé dans la table genres")



        # Inserts into production_actor
        # As ['cast'] column contains Nan values, we only keep the rows with string using isinstance()
        if isinstance(row['cast'], str):
            actors_row = [x.strip() for x in row["cast"].split(",")]  
        else:
            actors_row = []             
        for actor in actors_row:
            cursor.execute("SELECT id FROM actors WHERE name LIKE ?", (actor,))

            id_actor = cursor.fetchone()

            if id_actor:
                id_actor = id_actor[0]

                cursor.execute("INSERT INTO production_actor VALUES (?, ?)", (id_prod,id_actor))
                connection.commit()
            else:
                print(f"Name '{actor}' non trouvé dans la table actors")


    print("inserts done")


except Error as e:
    print(e)

finally:
    if connection:
        connection.close()

