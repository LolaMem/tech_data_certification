import pandas as pd
from datetime import datetime

# Fonction pour analyser la date, lorsqu'il y a 2 formats différents, même des valeurs vides
def parse_date(date_str):
    try:
        if isinstance(date_str, str):
            date_str = date_str.strip()
            if ',' in date_str:
                return datetime.strptime(date_str, "%B %d, %Y") # Format: November 1, 2019
            else:
                return datetime.strptime(date_str, "%d-%b-%y")  # Format: 14-Aug-20
    except (ValueError, TypeError):
        pass

    return pd.NaT  # Return NaT (Not a Time) for invalid or non-string values


# Fonction pour décomposer les données agrégées (par « , ») dans un champ d'un df de pandas
# et de les transformer en une liste de données individuelles.
def pd_series_to_list(df,column: str):
    final_list = []
    for item in df[column]:
        if pd.notna(item):  
            temp_list = [x.strip() for x in item.split(",")]
            new_items = [x for x in temp_list if x not in final_list]
            final_list.extend(new_items)
    return final_list