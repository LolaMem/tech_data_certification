import pandas as pd
import dash
from dash import dcc, html, dash_table, Dash
from dash.dependencies import Input, Output
import requests

#response = requests.get(f"http://127.0.0.1:8000/popular_productions?type={production_type}")
response = requests.get("http://127.0.0.1:8000/popular_productions")
data = response.json()
df = pd.json_normalize(data)

app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1("Most Popular Productions"),
    dcc.Dropdown(
        id='production-type',
        options=[
            {'label': 'Movies', 'value': 'Movie'},
            {'label': 'TV Shows', 'value': 'TV Show'}
        ],
        value='Movie'
    ),
    html.Table(id='top-productions-table')
])

@app.callback(
    Output('top-productions-table', 'children'),
    [Input('production-type', 'value')]
)
def update_table(production_type):
    response = requests.get(f"http://127.0.0.1:8000/popular_productions?type={production_type}")
    top_productions = response.json()

    rows = []
    for production in top_productions:
        rows.append(
            html.Tr([
                html.Td(production['Title']),
                html.Td(production['Year Released']),
                html.Td(production['Rating'])
            ])
        )

    return [
        html.Thead(html.Tr([html.Th("Title"), html.Th("Year Released"), html.Th("Rating")])),
        html.Tbody(rows)
    ]

if __name__ == '__main__':
    app.run_server(debug=True)