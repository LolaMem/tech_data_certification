
# Projet Netflix Lovers

Bienvenue dans le projet Netflix Lovers ! Ce projet a pour objectif de créer une application permettant aux utilisateurs de consulter les productions Netflix les plus populaires, d'explorer les genres les plus appréciés chaque année et de rechercher des films et séries par acteur.

## Les sources de données
Les sources utilisées pour ce projet incluent :

* **Netflix.csv** : Contient les détails des productions Netflix.
* **title.ratings.tsv** : Dataset d'IMDb contenant les évaluations des utilisateurs.
* **title.basics.tsv** : Dataset d'IMDb avec des informations complémentaires sur les titres.


**Note**: le fichier **title.basics.tsv** est très volumineux (+900Mb) et ne peut pas être stocké dans le dépôt git, il est nécessaire de télécharger les 2 fichiers IMDb (title.ratings.tsv.gz et title.basics.tsv.gz) de l'adresse indiquée ci-dessous et de les inclure dans le dossier du projet. [lien](https://datasets.imdbws.com/)

## Installation

* Cloner le dépôt :  
bash :  
  git clone https://gitlab.com/LolaMem/tech_data_certification.git  
  cd tech_data_certification

* Créer un environnement virtuel et installer les dépendances :  
bash :  
  python3 -m venv venv  
  source ./venv/bin/activate  
  pip install -r requirements.txt  

## Exécution
### Extraction et Nettoyage des Données

* Script : **extract_main.py**
* bash : python3 extract_main.py  
Lecture des sources, normalisation et nettoyage des dataframes.  
Écriture du dataframe final dans le fichier **NF_clean.csv**.

### Création de la Base de Données

* Scripts : **extract.py**
* bash : python3 extract.py  
Lecture du fichier **NF_clean.csv** pour créer un dataframe.  
Création de la base de données **db_NF.db**  
Modèle de la base de données illustré dans le fichier **netflix.png**

### Développement de l'API

* Script : **main_api.py**
* bash : uvicorn main_api:app --reload  
Exécution de l'API avec la commande $ fastapi dev ./main_api.py.  
Connexion à la base de données et mise à disposition des routes HTTP pour les requêtes.  

### Visualisation des Données

* Script : **view1.py**  
* bash : python3 view1.py  
Affichage d'une table des 10 productions les plus populaires, avec une sélection entre films et séries.  

* Script : **view2.py**  
* bash : python3 view2.py  
Diagramme en bâtons représentant le nombre de séries et de films ajoutés chaque année.  

* Script : **view3.py**  
* bash : python3 view3.py  
Dropdown des années affichant un diagramme en camembert avec les genres les plus populaires. 

## Utilisation de l'API

* Endpoint principal : /  
  Accueil de l'API.
  
* Endpoints spécifiques :  
  /popular_productions?type=movie : Affiche les 10 productions les plus populaires (films ou séries).  
  /productions_per_year : Affiche le nombre de productions ajoutées chaque année.  
  /genres_per_year : Affiche les genres les plus populaires chaque année.  
  /productions_per_actor/{actor} : Affiche les productions par acteur.
