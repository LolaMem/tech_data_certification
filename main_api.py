from fastapi import FastAPI
import sqlite3

#uvicorn main_api:app --reload
app = FastAPI(debug=True)

def get_connection():
    return sqlite3.connect("db_NF.db")

# Test root route
@app.get('/')
def home():
    return "Bienvenue au projet NETFLIX LOVERS"

# 1_Une route qui permet de récupérer les productions plus populaires
# http://127.0.0.1:8000/productions
@app.get("/popular_productions")
def most_popular_prod(type: str):

    connection = get_connection()
    cursor = connection.cursor()

    cursor.execute("""
        SELECT title, type, release_year, average_rating FROM productions
        WHERE type = ?
        ORDER BY average_rating DESC
        LIMIT 10
        """, (type,))
    
    rows = cursor.fetchall()
    
    top_productions = []
    for row in rows:
        production = {
            "Title" : row[0],
            "Type" : row[1],
            "Year Released" : row[2],
            "Rating" : row[3]
        }

        top_productions.append(production)

    return top_productions

    connection.close()


# 2_Une route qui permet de récupérer le nombre des productions ajoutée chaque année
# http://127.0.0.1:8000/productions_per_year
@app.get("/productions_per_year")
def productions_per_year():

    connection = get_connection()
    cursor = connection.cursor()

    cursor.execute("""
    SELECT type, added_year, count(*) AS total_productions 
    FROM productions 
    GROUP BY added_year, type              
    """)
    
    rows = cursor.fetchall()
    
    productions = []
    for row in rows:
        production = {
            "Type" : row[0],
            "Year added" : row[1],
            "Total Productions" : row[2]
        }

        productions.append(production)

    return productions

    connection.close()  

# 3 Une route qui permet de récupérer les genres plus populaires des productions ajoutées chaque année
# http://127.0.0.1:8000/genres_per_year
@app.get("/genres_per_year")
def genres_per_year():

    connection = get_connection()
    cursor = connection.cursor()

    cursor.execute("""
    SELECT ge.genre, pro.added_year, AVG(pro.average_rating) as rating
    FROM productions pro
    JOIN production_genre pg ON pro.id = pg.id_prod
    JOIN genres ge ON pg.id_genre = ge.id
    GROUP BY pro.added_year, ge.genre
    ORDER BY added_year ASC,rating  DESC""")
    
    rows = cursor.fetchall()
    
    genres = []
    for row in rows:
        genre = {
            "Genre" : row[0],
            "Year added" : row[1],
            "Average rating" : row[2]
        }

        genres.append(genre)

    return genres

    connection.close()  

# 4 Une route qui permet de récupérer les productions y compris un acteur spécifique
# http://127.0.0.1:8000/productions_per_actor
@app.get("/productions_per_actor/{actor}")
def productions_per_actor(actor: str):

    connection = get_connection()
    cursor = connection.cursor()

    cursor.execute("""
    SELECT pro.title, pro.type, pro.release_year, pro.average_rating, ac.name 
    FROM productions pro
    JOIN production_actor pa ON pro.id = pa.id_prod
    JOIN actors ac ON pa.id_person = ac.id
    WHERE ac.name LIKE ?
    ORDER BY ac.name""", (actor,))
    
    rows = cursor.fetchall()
    
    productions = []
    for row in rows:
        production = {
            "Title" : row[0],
            "Type" : row[1],
            "Year Released" : row[2],
            "Rating" : row[3],
            "Name" : row[4]
        }

        productions.append(production)

    return productions

    connection.close()  

if __name__ == '__main_api__':
    print("main_api executed")