import pandas as pd
import numpy as np
from tools import parse_date

# Read the data with specified dtypes
dff1 = pd.read_table("title.ratings.tsv", delimiter='\t', dtype={'tconst': 'object', 'averageRating': 'float32', 'numVotes': 'int32'})
dff2 = pd.read_table("title.basics.tsv", delimiter='\t', low_memory=False)

# Drop unnecessary columns (example: dropping 'originalTitle' and 'isAdult')
dff1.drop(columns=['numVotes'], inplace=True)
dff2.drop(columns=['isAdult'], inplace=True)

# Drop rows with invalid titleType
#titleType = ['tvEpisode', 'short', 'movie', 'video', 'tvSeries', 'tvMovie','tvMiniSeries', 'tvSpecial', 'videoGame', 'tvShort', 'tvPilot']
invalid_types = ['video','videoGame','tvPilot']
dff2 = dff2[~(dff2["titleType"].isin(invalid_types))]

# Drop rows with 'Episode #' in primaryTitle
dff2 = dff2[~dff2['primaryTitle'].str.contains("Episode #", na=False)]

# Replace '\N' with NaN, in columns startYear, endYear and runtimeMinutes
dff2.replace('\\N', np.nan, inplace=True)

# Specify data types for the columns to avoid mixed types
dff2 = dff2.astype({
    'tconst': 'object',
    'titleType': 'category',  # use category dtype for strings with limited unique values
    'primaryTitle': 'object',
    'startYear': 'object', 
    'endYear': 'object',  
    'runtimeMinutes': 'object', # Can't be int as there are cells with text
    'genres': 'object'
})

dff2['runtimeMinutes'] = pd.to_numeric(dff2['runtimeMinutes'], errors='coerce')
dff2['runtimeMinutes'].fillna(0).astype(int)

# Merge dataframes
df_imdb = pd.merge(dff2, dff1, how='left', on=['tconst'])

##########################################################
# read Netflix dataset
df_nf = pd.read_csv("NetFlix.csv")  # 7787 lignes
df_nf.info()

df_nf.drop(columns=['country', 'rating', 'description'], inplace=True)

# Appliquer la fonction parse_date pour normaliser les valeurs car les valeurs dans "date_added"
# ont 2 formats de date différents, plus des valeurs vides
df_nf['new_added_date'] = df_nf['date_added'].apply(parse_date)

# Extract l'année de la date pour les inserts dans sqlite (date type not supported, and easy for the views with selection by year)
# fillna(0) is used to fill with 0 any NaN values (resulting from invalid dates) before converting to integers using .astype(int)
df_nf['year_added'] = df_nf['new_added_date'].dt.year.fillna(0).astype(int)
df_nf['release_year'] = df_nf['release_year'].astype(str)

df_nf["endYear"] = df_nf["release_year"]
df_nf["startYear"] = df_nf["release_year"]

# Division of df_nf into 2, according to categories in type: "Movies" or "TV Show", to facilitate the merge with the IMDb df
df_nf_movies = df_nf[df_nf['type'] == "Movie"]      # 5377 lignes
df_nf_series = df_nf[df_nf['type'] == "TV Show"]    # 2410 lignes

#############################################################################
# MERGING df NF Movies & IMDb

# Normalisation of column names to use them as keys for the merge
df_imdb.rename(columns={"primaryTitle":"title", "runtimeMinutes":"duration", 'genres':'genres_imdb'}, inplace=True)

# Merge the df on multiple keys
df_movies = pd.merge(df_nf_movies, df_imdb, how= 'left', on=['title', 'duration', 'startYear'])

df_movies.drop_duplicates(subset=["title"], inplace=True)

#############################################################################
# MERGING df NF Series & IMDb

df_series = pd.merge(df_nf_series, df_imdb, how= 'left', on=['title', 'endYear'])
df_series.info()    

df_series.drop_duplicates(subset=["title"], inplace=True)

#####################################################################

df_total = pd.concat([df_movies, df_series], axis=0)
print(df_total.info())

df_total.drop(columns= ['director','date_added','duration', 'new_added_date', 'endYear','endYear_x','endYear_y','tconst','titleType',
                        'originalTitle', 'startYear','startYear_x','startYear_y','genres_imdb','duration_x','duration_y'], inplace=True)

print(df_total.info())
df_total.to_csv('NF_clean.csv', index=False, mode="w")

