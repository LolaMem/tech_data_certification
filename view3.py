from dash import Dash, dcc, html, Input, Output, callback
import pandas as pd
import requests
import plotly.express as px

response = requests.get("http://127.0.0.1:8000/genres_per_year")
data = response.json()

df = pd.json_normalize(data)
df["Average rating"] = df["Average rating"].round(2)
years = df['Year added'].unique()

# Creation de df qui ne stocke que les 5 genres le plus populaire (max Average rating) par année
df_final = pd.DataFrame()

for year in years:
    top_genres = df[df['Year added'] == year].nlargest(5, 'Average rating')
    df_final = pd.concat([df_final, top_genres])

app = Dash(__name__)

app.layout = html.Div([
    html.H1("Most Popular Genres per Year"),
    dcc.Dropdown(
        id='year',
        options=[{'label':i, 'value':i} for i in years],
        value = years[-1]
        ),
    dcc.Graph(id = "pie_chart")
])

@app.callback(
    Output('pie_chart', 'figure'),
    [Input('year', 'value')]
)
def update_chart(selected_year):
    df_filtered = df_final[df_final['Year added'] == selected_year]
    fig = px.pie(df_filtered, names = "Genre", values = "Average rating", title = f"Most popular genres in {selected_year}")
 
    fig.update_traces(
        textinfo='percent+value',  # Show percent, label, and value
        textposition='outside',  # Position the text outside the pie chart
        automargin=True,  # Adjust margins automatically
        showlegend=True  # Hide the legend to avoid redundancy
    )
    fig.update_layout(margin=dict(t=50, b=50, l=50, r=50))  # Remove margins

    return fig

if __name__ == '__main__':
    app.run_server(debug=True)