from dash import Dash, dcc, html, Output
import pandas as pd
import requests
import plotly.express as px

# http://127.0.0.1:8000/genres_per_year
response = requests.get("http://127.0.0.1:8000/productions_per_year")
data = response.json()

df = pd.json_normalize(data)

# Missing values for added date were first converted to 0, 
# in the representation ths scale between 0 and 2008 is too big, and data in years 2008 to 2021 very small
# Replace 0 with 2007, but not real year

df.loc[0, "Year added"] = 2007
fig = px.bar(df, x="Year added", y="Total Productions",color="Type", barmode="group")

app = Dash(__name__)


app.layout = html.Div([
    html.H4('Productions per year'),
    dcc.Graph(id="graph", figure = fig),
])


if __name__ == '__main__':
    app.run_server(debug=True)